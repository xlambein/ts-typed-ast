# Tree-sitter typed AST generator

This crate provides a build script function for generating a typed AST from a tree-sitter grammar. Inspired by [rowan](https://github.com/rust-analyzer/rowan).

## Setup

In your project's `Cargo.toml`:

```toml
[dependencies]
tree-sitter-your-lang = { path = "/path/to/tree-sitter-your-lang" }
ts-typed-ast = "0.1"

[build-dependencies]
tree-sitter-your-lang = { path = "/path/to/tree-sitter-your-lang" }
ts-typed-ast = "0.1"
```

In your project's build script (located at `build.rs`):

```rust
fn main() {
    // Generate typed AST from the tree-sitter grammar
    let out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR").unwrap());
    ts_typed_ast::generate(
        &tree_sitter_your_lang::language(),
        tree_sitter_your_lang::NODE_TYPES,
        std::fs::File::create(out_dir.join("ast.rs")).unwrap(),
    );
}
```

Finally, add a module to your project's source (e.g. `src/ast.rs`) with the following:

```rust
include!(concat!(env!("OUT_DIR"), "/ast.rs"));
```

## Generating the AST

For each named rule in the grammar, a Rust `struct` named with this rule is generated. For example, the following rule:

```js
{
  function_application: $ => seq(
    field('function', $.identifier),
    '(',
    terminated(',', field('arguments', $._expression)),
    ')',
  ),
}
```

produces the Rust code:

```rust
#[derive(Debug, Clone, Copy)]
pub struct FunctionApplication<'tree> { /* ... */ }

impl<'tree> ::ts_typed_ast::AstNode<'tree> for FunctionApplication<'tree> {
    fn can_cast(kind: u16) -> bool { /* ... */ }
    fn cast(node: ::tree_sitter::Node<'tree>) -> Option<Self> { /* ... */ }
    fn node(&self) -> ::tree_sitter::Node<'tree> { /* ... */ }
}

impl<'tree> FunctionApplication<'tree> {
    pub fn function(&self) -> Result<Identifier<'tree>, ::ts_typed_ast::MissingNodeChildError<'tree>> { /* ... */ }
    pub fn arguments(&self) -> impl Iterator<Item = Expression<'tree>> { /* ... */ }
}
```

The methods `cast` and `node` of the `AstNode` trait can be used to go back-and-forth between `tree_sitter::Node` and the typed AST. Then, specific methods `function` and `arguments` grant access to specific fields of the rule. Note that such functions are only generated when `field('func_name', $.item)` is used.

The grammar's supertypes each generate a Rust `enum`. For example, the following:

```js
{
  supertypes: $ => [$._expression],
  rules: {
    _expression: $ => choice(
      $.identifier,
      $.lambda,
      $.function_application,
    ),
  },
}
```

will generate:

```rust
#[derive(Debug, Clone, Copy)]
pub enum Expression<'tree> {
    FunctionApplication(FunctionApplication<'tree>),
    Lambda(Lambda<'tree>),
    Identifier(Identifier<'tree>),
}

impl<'tree> ::ts_typed_ast::AstNode<'tree> for Expression<'tree> {
    fn can_cast(kind: u16) -> bool { /* ... */ }
    fn cast(node: ::tree_sitter::Node<'tree>) -> Option<Self> { /* ... */ }
    fn node(&self) -> ::tree_sitter::Node<'tree> { /* ... */ }
}
```

Enums will also produce `{node}Visitor` traits with methods to visit each possible node of the enum:

```rust
pub trait ExpressionVisitor<'tree> {
    type Output;

    fn expression(&mut self, expression: Expression<'tree>) {
        match expression {
            Expression::FunctionApplication(function_application) =>
                self.function_application(function_application),
            // etc.
        }
    }

    fn function_application(&mut self, function_application: FunctionApplication<'tree>)
        -> Self::Output;
    // etc.
}
```

Finally, unnamed `choice`s generate `enum`s on the fly. For example:

```js
{
  neg: $ => seq(
    '-',
    field('value', choice(
      $.integer,
      $.float,
    )),
  ),
}
```

will produce:

```rust
#[derive(Debug, Clone, Copy)]
pub struct Neg<'tree> { /* ... */ }

impl<'tree> ::ts_typed_ast::AstNode<'tree> for Neg<'tree> { /* ... */ }

impl<'tree> Neg<'tree> { /* ... */ }

#[derive(Debug, Clone, Copy)]
pub enum NegValue<'tree> {
    Integer(Integer<'tree>),
    Float(Float<'tree>),
}

impl<'tree> ::ts_typed_ast::AstNode<'tree> for NegValue<'tree> {
    fn can_cast(kind: u16) -> bool { /* ... */ }
    fn cast(node: ::tree_sitter::Node<'tree>) -> Option<Self> { /* ... */ }
    fn node(&self) -> ::tree_sitter::Node<'tree> { /* ... */ }
}
```

## Using the AST

You'll probably want to lower the typed AST into an intermediate representation which is easier to work with. For example:

```rust
mod ir {
    pub struct FunctionApplication {
        pub function: ResolvedIdentifier,
        pub arguments: Vec<Argument>,
    }

    // ...
}

mod lowering {
    fn lower_function_application(state: &mut State, it: ast::FunctionApplication) -> Result<ir::FunctionApplication, LoweringError> {
        let function = state.resolve_identifier(it.function()?);
        let arguments = it.arguments().map(|it| {
            lower_argument(state, it)
        }).collect::<Result<Vec<_>, _>>()?;
        Ok(ir::FunctionApplication { function, arguments })
    }

    // ...
}
```

## Why not a proc macro?

Because you can inspect the generated code more easily, either by manually opening the file `./target/debug/build/[crate-name]-[hash]/out/ast.rs`, or more practically by jumping to definitions in your LSP.
