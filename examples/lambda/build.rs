fn main() {
    // Generate typed AST from the tree-sitter grammar
    let out_dir = std::path::PathBuf::from(std::env::var("OUT_DIR").unwrap());
    ts_typed_ast::generate(
        &tree_sitter_lambda::language(),
        tree_sitter_lambda::NODE_TYPES,
        std::fs::File::create(out_dir.join("ast.rs")).unwrap(),
    );
}
