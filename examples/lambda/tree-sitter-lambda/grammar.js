module.exports = grammar({
  name: 'lambda',

  extras: $ => [
    $.comment,
    /\s/,
  ],

  conflicts: $ => [],

  supertypes: $ => [
    $._expr,
  ],

  rules: {
    source_file: $ => field('expr', $._expr),

    comment: $ => token(seq('--', /.*/)),

    _expr: $ => choice(
      $.ident,
      $.bool,
      $.int,
      $.lambda,
      $.appl,
      $.let,
      $.cond,
      $.paren,
    ),

    ident: $ => token(/[a-zA-Z_][a-zA-Z0-9_]*/),

    bool: $ => choice('false', 'true'),

    int: $ => token(/-?\d+/),

    lambda: $ => prec.left(1, seq(
      choice('λ', '\\'),
      field('var', $.ident),
      '.',
      field('body', $._expr),
    )),

    appl: $ => prec.left(2, seq(
      field('lambda', $._expr),
      field('arg', $._expr),
    )),

    let: $ => seq(
      'let',
      field('var', $.ident),
      '=',
      field('value', $._expr),
      'in',
      field('body', $._expr),
    ),

    cond: $ => seq(
      'if',
      field('cond', $._expr),
      'then',
      field('then', $._expr),
      'else',
      field('else', $._expr),
    ),

    paren: $ => seq(
      '(',
      field('expr', $._expr),
      ')',
    ),
  }
});

function sep(separator, rule) {
  return optional(sep1(separator, rule));
}

function sep1(separator, rule) {
  return seq(rule, repeat(seq(separator, rule)));
}

function terminated(separator, rule) {
  return optional(terminated1(separator, rule));
}

function terminated1(separator, rule) {
  return seq(rule, repeat(seq(separator, rule)), optional(separator));
}

