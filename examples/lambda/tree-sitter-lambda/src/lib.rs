//! This crate provides lambda language support for the [tree-sitter][] parsing library.
//!
//! Typically, you will use the [language][language func] function to add this language to a
//! tree-sitter [Parser][], and then use the parser to parse some code:
//!
//! ```
//! let code = "";
//! let mut parser = tree_sitter::Parser::new();
//! parser.set_language(&tree_sitter_lambda::language()).expect("Error loading lambda grammar");
//! let tree = parser.parse(code, None).unwrap();
//! ```
//!
//! [Language]: https://docs.rs/tree-sitter/*/tree_sitter/struct.Language.html
//! [language func]: fn.language.html
//! [Parser]: https://docs.rs/tree-sitter/*/tree_sitter/struct.Parser.html
//! [tree-sitter]: https://tree-sitter.github.io/

use tree_sitter::Language;

extern "C" {
    fn tree_sitter_lambda() -> Language;
}

/// Get the tree-sitter [Language][] for this grammar.
///
/// [Language]: https://docs.rs/tree-sitter/*/tree_sitter/struct.Language.html
pub fn language() -> Language {
    unsafe { tree_sitter_lambda() }
}

/// The content of the [`node-types.json`][] file for this grammar.
///
/// [`node-types.json`]: https://tree-sitter.github.io/tree-sitter/using-parsers#static-node-types
pub const NODE_TYPES: &'static str = include_str!(concat!(env!("OUT_DIR"), "/src/node-types.json"));

// Uncomment these to include any queries that this grammar contains

// pub const HIGHLIGHTS_QUERY: &'static str = include_str!("../../queries/highlights.scm");
// pub const INJECTIONS_QUERY: &'static str = include_str!("../../queries/injections.scm");
// pub const LOCALS_QUERY: &'static str = include_str!("../../queries/locals.scm");
// pub const TAGS_QUERY: &'static str = include_str!("../../queries/tags.scm");

#[cfg(test)]
mod tests {
    use std::path::{Path, PathBuf};

    use tree_sitter_cli::test::TestOptions;

    #[test]
    fn test_can_load_grammar() {
        let mut parser = tree_sitter::Parser::new();
        parser
            .set_language(&super::language())
            .expect("Error loading lambda language");
    }

    #[test]
    fn test_corpus() {
        let update = std::env::var("TREE_SITTER_TEST_UPDATE").map_or(false, |var| var == "1");
        let mut parser = tree_sitter::Parser::new();
        parser.set_language(&super::language()).unwrap();
        tree_sitter_cli::test::run_tests_at_path(
            &mut parser,
            &mut TestOptions {
                output: &mut String::new(),
                path: PathBuf::from("test/corpus"),
                debug: false,
                debug_graph: false,
                include: None,
                exclude: None,
                file_name: None,
                update,
                open_log: false,
                languages: std::collections::BTreeMap::from_iter([("lambda", &super::language())]),
                color: true,
                test_num: 0,
                parse_rates: &mut Vec::new(),
                stat_display: tree_sitter_cli::test::TestStats::All,
                stats: &mut tree_sitter_cli::parse::Stats::default(),
                show_fields: true,
                overview_only: false,
            },
        )
        .expect("corpus tests failed");
    }

    #[test]
    fn test_queries() {
        tree_sitter_cli::test::check_queries_at_path(&super::language(), &Path::new("queries"))
            .expect("invalid queries");
    }
}
