use std::env;
use std::path::{Path, PathBuf};

fn main() {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    let grammar_path = "grammar.js";

    if let Err(err) = tree_sitter_generate::generate_parser_in_directory(
        Path::new("."),
        Some(
            out_dir
                .join("src")
                .to_str()
                .expect("OUT_DIR is not a valid UTF-8 path"),
        ),
        Some(grammar_path),
        tree_sitter::LANGUAGE_VERSION,
        None,
        None,
    ) {
        panic!("Could not generate grammar:\n{err}")
    }

    let mut c_config = cc::Build::new();
    c_config.include(&out_dir);
    c_config
        // Fix for https://github.com/tree-sitter/tree-sitter/issues/2531
        .flag_if_supported("-O3")
        .flag_if_supported("-Wno-unused-parameter")
        .flag_if_supported("-Wno-unused-but-set-variable")
        .flag_if_supported("-Wno-trigraphs");
    let parser_path = out_dir.join("src/parser.c");
    c_config.file(&parser_path);

    // If your language uses an external scanner written in C,
    // then include this block of code:

    /*
    let scanner_path = src_dir.join("scanner.c");
    c_config.file(&scanner_path);
    println!("cargo:rerun-if-changed={}", scanner_path.to_str().unwrap());
    */

    c_config.compile("parser");
    println!("cargo:rerun-if-changed={}", grammar_path);
}
