use std::{fmt::Write, num::ParseIntError, rc::Rc};

use crate::ast::{self, ExprVisitor};
use ts_typed_ast::{AstNode, MissingNodeChildError};

pub fn eval(source: &[u8]) -> std::result::Result<Value, String> {
    let mut parser = tree_sitter::Parser::new();
    parser
        .set_language(&tree_sitter_lambda::language())
        .unwrap();
    let tree = parser.parse(source, None).unwrap();
    let mut errors = vec![];
    collect_errors(&mut errors, &mut tree.walk());
    if errors.is_empty() {
        let mut eval = Evaluator::new(source);
        let expr = ast::SourceFile::cast(tree.root_node()).unwrap();
        match eval.source_file(expr) {
            Ok(value) => return Ok(value),
            Err(err) => errors.push(err),
        }
    }
    let mut message = String::new();
    for err in errors {
        let start = source[..err.node.start_byte()]
            .iter()
            .rposition(|&c| c == b'\n')
            .map(|i| i + 1)
            .unwrap_or(0);
        let end = source[err.node.end_byte()..]
            .iter()
            .position(|&c| c == b'\n')
            .map(|i| err.node.end_byte() + i)
            .unwrap_or(source.len());
        let context = std::str::from_utf8(&source[start..end]).unwrap_or_default();
        write!(
            &mut message,
            r#"{}
line {}, column {}:

{}
"#,
            err.inner,
            err.node.start_position().row + 1,
            err.node.start_position().column + 1,
            context,
        )
        .expect("failed to format error message");
    }
    Err(message)
}

fn collect_errors<'tree>(
    errors: &mut Vec<Loc<'tree, EvalError>>,
    cursor: &mut tree_sitter::TreeCursor<'tree>,
) {
    let node = cursor.node();
    if node.is_error() {
        errors.push(Loc::new(EvalError::SyntaxError, node));
    } else if node.is_missing() {
        // TODO should it be a "missing" error instead?
        errors.push(Loc::new(EvalError::SyntaxError, node));
    }
    if !cursor.goto_first_child() {
        return;
    }
    loop {
        collect_errors(errors, cursor);
        if !cursor.goto_next_sibling() {
            break;
        }
    }
    cursor.goto_parent();
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Value {
    Bool(bool),
    Int(i64),
    Lambda(LambdaId),
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Value::Bool(b) => write!(f, "{b}"),
            Value::Int(i) => write!(f, "{i}"),
            Value::Lambda(l) => write!(f, "<lambda #{}>", l.0),
        }
    }
}

#[derive(Debug)]
pub struct Loc<'tree, T> {
    inner: T,
    node: tree_sitter::Node<'tree>,
}

impl<'tree, T> Loc<'tree, T> {
    fn new(inner: T, node: tree_sitter::Node<'tree>) -> Self {
        Self { inner, node }
    }
}

#[derive(Debug)]
pub enum EvalError {
    SyntaxError,
    ParseIntError(ParseIntError),
    TypeError { actual: Value, expected: String },
    UnboundIdentifier(String),
}

type Result<'tree> = std::result::Result<Value, Loc<'tree, EvalError>>;

struct Evaluator<'src, 'tree> {
    source: &'src [u8],
    strings: Vec<String>,
    lambdas: Vec<Lambda<'tree>>,
    env: Rc<Env>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct StrId(usize);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct LambdaId(usize);

impl<'tree, T: std::fmt::Display> std::fmt::Display for Loc<'tree, T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let start = self.node.start_position();
        write!(
            f,
            "line {}, col {}: {}",
            start.row, start.column, self.inner
        )
    }
}

impl<'tree, T: std::error::Error> std::error::Error for Loc<'tree, T> {}

impl EvalError {
    fn located<'tree>(self, node: impl AstNode<'tree>) -> Loc<'tree, Self> {
        Loc::new(self, node.node())
    }
}

impl std::fmt::Display for EvalError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EvalError::SyntaxError => write!(f, "syntax error"),
            EvalError::ParseIntError(err) => err.fmt(f),
            EvalError::TypeError { actual, expected } => {
                write!(f, "expected {expected}, found `{actual}`")
            }
            EvalError::UnboundIdentifier(ident) => {
                write!(f, "unbound identifier '{ident}'")
            }
        }
    }
}

impl std::error::Error for EvalError {}

// Note: in practice, we should never produce a syntax error when evaluating,
// because we catch them all beforehand. Still, we don't `unwrap` potential
// syntax errors, in order to stay robust to an error in `tree-sitter` or our
// own code.
impl<'tree> From<MissingNodeChildError<'tree>> for Loc<'tree, EvalError> {
    fn from(err: MissingNodeChildError<'tree>) -> Self {
        if let Some(child) = err.node.child_by_field_id(err.field_id) {
            Loc::new(EvalError::SyntaxError, child)
        } else {
            Loc::new(EvalError::SyntaxError, err.node)
        }
    }
}

impl From<ParseIntError> for EvalError {
    fn from(err: ParseIntError) -> Self {
        EvalError::ParseIntError(err)
    }
}

#[derive(Default)]
enum Env {
    #[default]
    Empty,
    Binding {
        ident: StrId,
        value: Value,
        prev: Rc<Env>,
    },
}

#[derive(Clone)]
struct Lambda<'tree> {
    var: StrId,
    env: Rc<Env>,
    body: ast::Expr<'tree>,
}

impl Env {
    fn push(self: Rc<Self>, ident: StrId, value: Value) -> Rc<Self> {
        Rc::new(Env::Binding {
            ident,
            value,
            prev: self,
        })
    }

    fn iter(&self) -> impl Iterator<Item = (StrId, &Value)> + '_ {
        struct EnvIterator<'a> {
            env: &'a Env,
        }

        impl<'a> Iterator for EnvIterator<'a> {
            type Item = (StrId, &'a Value);

            fn next(&mut self) -> Option<Self::Item> {
                match self.env {
                    Env::Empty => None,
                    Env::Binding { ident, value, prev } => {
                        self.env = &**prev;
                        Some((*ident, value))
                    }
                }
            }
        }

        EnvIterator { env: self }
    }
}

impl<'src, 'tree> Evaluator<'src, 'tree> {
    fn new(source: &'src [u8]) -> Self {
        Self {
            source,
            strings: vec![],
            lambdas: vec![],
            env: Default::default(),
        }
    }

    fn source_file(&mut self, source_file: ast::SourceFile<'tree>) -> Result<'tree> {
        self.expr(source_file.expr()?)
    }

    fn intern(&mut self, s: &str) -> StrId {
        // TODO make this faster than O(n)
        match self.strings.iter().enumerate().find(|&(_, it)| it == s) {
            Some((i, _)) => StrId(i),
            None => {
                self.strings.push(s.to_owned());
                StrId(self.strings.len() - 1)
            }
        }
    }
}

impl<'src, 'tree> ExprVisitor<'tree> for Evaluator<'src, 'tree> {
    type Output = Result<'tree>;

    fn ident(&mut self, ident: ast::Ident<'tree>) -> Self::Output {
        let s = ident
            .utf8_text(self.source)
            .expect("parser accepted non-UTF-8 token");
        let var = self.intern(s);
        match self.env.iter().find(|&(name, _)| name == var) {
            Some((_, value)) => Ok(value.clone()),
            None => Err(EvalError::UnboundIdentifier(s.to_owned()).located(ident)),
        }
    }

    fn bool(&mut self, bool: ast::Bool<'tree>) -> Self::Output {
        let value = match bool
            .utf8_text(self.source)
            .expect("parser accepted non-UTF-8 token")
        {
            "true" => true,
            "false" => false,
            _ => unreachable!(),
        };
        Ok(Value::Bool(value))
    }

    fn int(&mut self, int: ast::Int<'tree>) -> Self::Output {
        let value: i64 = int
            .utf8_text(self.source)
            .expect("parser accepted non-UTF-8 token")
            .parse()
            .map_err(|err| EvalError::from(err).located(int))?;
        Ok(Value::Int(value))
    }

    fn lambda(&mut self, lambda: ast::Lambda<'tree>) -> Self::Output {
        let var = self.intern(
            lambda
                .var()?
                .utf8_text(&self.source)
                .expect("parser accepted non-UTF-8 token"),
        );
        let body = lambda.body()?;
        let lambda = LambdaId(self.lambdas.len());
        self.lambdas.push(Lambda {
            var,
            env: self.env.clone(),
            body,
        });
        Ok(Value::Lambda(lambda))
    }

    fn appl(&mut self, appl: ast::Appl<'tree>) -> Self::Output {
        let lambda_node = appl.lambda()?;
        let lambda = self.expr(lambda_node)?;
        let Value::Lambda(lambda) = lambda else {
            return Err(EvalError::TypeError {
                actual: lambda,
                expected: String::from("a lambda"),
            }
            .located(lambda_node));
        };
        let Lambda { var, env, body } = self.lambdas[lambda.0].clone();
        let arg = self.expr(appl.arg()?)?;
        let prev_env = std::mem::replace(&mut self.env, env.push(var, arg));
        let ret = self.expr(body);
        self.env = prev_env;
        ret
    }

    fn r#let(&mut self, let_: ast::Let<'tree>) -> Self::Output {
        let var = self.intern(
            let_.var()?
                .utf8_text(&self.source)
                .expect("parser accepted non-UTF-8 token"),
        );
        let value = self.expr(let_.value()?)?;
        let body = let_.body()?;
        let env = self.env.clone().push(var, value);
        let prev_env = std::mem::replace(&mut self.env, env);
        let ret = self.expr(body);
        self.env = prev_env;
        ret
    }

    fn cond(&mut self, cond: ast::Cond<'tree>) -> Self::Output {
        let cond_node = cond.cond()?;
        let test = self.expr(cond_node)?;
        match test {
            Value::Bool(true) => self.expr(cond.then()?),
            Value::Bool(false) => self.expr(cond.r#else()?),
            _ => Err(EvalError::TypeError {
                actual: test,
                expected: String::from("a boolean"),
            }
            .located(cond_node)),
        }
    }

    fn paren(&mut self, paren: ast::Paren<'tree>) -> Self::Output {
        self.expr(paren.expr()?)
    }
}
