mod ast {
    include!(concat!(env!("OUT_DIR"), "/ast.rs"));
}

pub mod eval;

pub use eval::{eval, Value};

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn eval_success() {
        let source = br#"
            let oneOrTwo = \x. if x then 1 else 2 in
            let id = \x.x in
            id (oneOrTwo true)
        "#;
        assert_eq!(eval(source), Ok(Value::Int(1)));
    }

    #[test]
    fn eval_syntax_error() {
        let source = br#"let x = 123 in"#;
        assert_eq!(
            eval(source),
            Err(String::from(
                r#"syntax error
line 1, column 15:

let x = 123 in
"#
            ))
        );

        let source = br#"let = 123 in x"#;
        assert_eq!(
            eval(source),
            Err(String::from(
                r#"syntax error
line 1, column 1:

let = 123 in x
"#
            ))
        );
    }

    #[test]
    fn eval_parse_int_error() {
        let source = br#"123456789123456789123456789"#;
        assert_eq!(
            eval(source),
            Err(String::from(
                r#"number too large to fit in target type
line 1, column 1:

123456789123456789123456789
"#
            ))
        );
    }

    #[test]
    fn eval_type_error() {
        let source = br#"if 1 then 2 else 3"#;
        assert_eq!(
            eval(source),
            Err(String::from(
                r#"expected a boolean, found `1`
line 1, column 4:

if 1 then 2 else 3
"#
            ))
        );
    }

    #[test]
    fn eval_unbound_identifier() {
        let source = br#"let id = \x.y in id 1"#;
        assert_eq!(
            eval(source),
            Err(String::from(
                r#"unbound identifier 'y'
line 1, column 13:

let id = \x.y in id 1
"#
            ))
        );
    }
}
